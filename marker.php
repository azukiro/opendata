<?php
class Marker{
    private $_x;
    private $_y;
    private $_name;
    private $_uai;
    private $_website;

    function __construct($_x,$_y,$_name,$_website,$_uai)
    {
    
        $this->_x=$_x;
        $this->_y=$_y;
        $this->_name=$_name;
        $this->_uai=$_uai;
        $this->_website=$_website;
    }

   

    //https://www.google.com/maps/search/google+maps+
    function getMarker(){
        //Fonction pour créer les markers la carte

        //Association de la couleur unique de l'university à l'uai comme dans formation.php
        $r=substr($this->_uai,0,2);
        $g=substr($this->_uai,2,2);
        $b=substr($this->_uai,4,2);
        $color="#$b$g$r";

        echo "<script>
        function  getLocate(x,y){
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((function(position) {
             //Si la geolocalisation est accepté on redirige vers maps avec l'itinéraire
             window.open('https://www.google.com/maps/dir/'+x+','+y+'/'+position.coords.latitude+','+position.coords.longitude);
            }));
           
        } else {
            //sinon juste avec le point d'arrivé
         window.open('https://www.google.com/maps/dir/'+x+','+y);
        }
     }
     </script>";
        if (isset( $this->_website) && $this->_website!="") {
          
            return " <script >
            var marker = L.marker([ $this->_x,  $this->_y]).addTo(mymap);
                marker.bindPopup(\"<b>$this->_name <br><a href=' $this->_website' target='_blank'>Site</a><br><button onClick='getLocate($this->_x,$this->_y);' >Itinéraire</button></b> \").openPopup();
                marker.valueOf()._icon.style.backgroundColor='$color';
            </script>";
        } else {
    
            return " <script >
            var marker = L.marker([ $this->_x,  $this->_y]).addTo(mymap);
                marker.bindPopup(\"<b>$this->_name</b><br><button onClick='getLocate($this->_x,$this->_y);' >Itinéraire</button></b>\").openPopup();
                marker.valueOf()._icon.style.backgroundColor='$color'
            </script>";
        }
    }


}

<!DOCTYPE html>
<html lang="fr">

<head>
    <title>A la poursuite de tes études</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin="" />

    <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>

</head>

<?php
include("connexion.inc.php");
require_once("formation.php");
require_once("marker.php");
session_start();
function addSet($array, $element)
{

    if (!in_array($element, $array)) {
        $array[] = $element;
    }
    return $array;
}

function addSetWithKey($array, $key, $value)
{
    if (!in_array($value, $array)) {
        $array[$key] = $value;
    }
    return $array;
}

function lastFormation($array)
{

    foreach ($array as $key => $value) {

        if (count($value) > 1) {

            return false;
        }
    }
    return true;
}

function marker($row)
{
    if (!isset($row['coordonnees'])) {
        return;
    }

    $x = $row['coordonnees'][0];
    $y = $row['coordonnees'][1];
    $uniname = $row['uo_lib'];
    if (isset($row['url'])) {

        $website = $row['url'];
    } else {
        $website = "";
    }

    $mark = new Marker($x, $y, $uniname, $website, $row['uai']);


    echo $mark->getMarker();
}

function issetOrdefault($value)
{
    return isset($value) ? $value : "";
}

function createFormation($value)
{
    return new Formation(issetOrdefault($value["sect_disciplinaire_lib"]), issetOrdefault($value["gd_disciscipline_lib"]),  issetOrdefault($value["diplome_lib"]), issetOrdefault($value["niveau_lib"]), issetOrdefault($value["com_etab_lib"]), issetOrdefault($value["dep_etab_lib"]), issetOrdefault($value["reg_etab_lib"]), issetOrdefault($value["effectif_total"]), issetOrdefault($value["element_wikidata"]), issetOrdefault($value["etablissement_lib"]), issetOrdefault($value["etablissement"]));
}
function lauchApi($link)
{
    $contents = file_get_contents($link);

    $contents = json_decode($contents, true);
    if ($contents == null || $contents == '') {
        echo ('Connexion impossible à l\'API !');
    }
    return $contents;
}

function updateOrInsertUai($uai){
    include("parametre.inc.php");
    try{
        $connexion="mysql:host=$host;dbname=$db;charset=utf8;";
        $bd = new PDO($connexion, $user, $pwd,array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION)); 
    }catch(Exception $e){
        die('Connexion impossible à la base de données !'.$e->getMessage());
    }
    
    $insert="INSERT INTO universite VALUES ('$uai',1)";
    $select="SELECT nbClick FROM universite WHERE uai='$uai';";
    $result=$bd->query($select);
    $insert=true;
    $nb=0;
    while($ligne=$result->fetch(PDO::FETCH_NUM)){
        $insert=false;
        $nb=$ligne[0];
    }
    $nb+=1;
    if($insert){
        $req="INSERT INTO universite VALUES ('$uai',1)";
    }else{
        $req="UPDATE universite SET nbClick=$nb WHERE uai='$uai'";
    }
    $result=$bd->query($req);

  
    
}
if(isset($_GET["uaiInc"]) && $_GET["uaiInc"] != ""){
    $uai=$_GET["uaiInc"];
    $url=$_GET["uaiIncWebSite"];
    updateOrInsertUai($uai);
    echo "<script>window.open(\"".$url."\");</script>";
}

if (isset($_GET["deleteType"]) && $_GET["deleteType"] != "") {

    //Si on supprime un élement du filtrage en cours
    if (!isset($_SESSION["delete"])) {
        $_SESSION["delete"] = array(
            "Discipline" => array(), "Grande_Discipline" => array(),
            "Niveau" => array(), "Diplome" => array()
        );
    }




    $deleteType = $_GET["deleteType"];
    $deleteValue = $_GET["delete"];
    $_SESSION["delete"][$deleteType] = addSet($_SESSION["delete"][$deleteType], $deleteValue);
    $arraydel = $_SESSION["delete"];
    $Discipline2 = array();
    $Etablissement2 = array();
    $Grande_Disciple2 = array();
    $Niveau2 = array();
    $Diplome2 = array();
    $formation = array();
    $etab = array();
    $nbRow = 0;

    foreach ($_SESSION["formation"] as $key1 => $row) {


        if (!(in_array($row->_discipline, $_SESSION["delete"]["Discipline"]))) { //On verifie que la vleur nous intéresse et si elle peut être garder on la garde sinon on ne s'en occupe plus

            $nivset = false;
            $gdiset = false;
            $dipset = false;
            if (((!isset($_GET["niv"]) || $niv == "") || (isset($_GET["niv"]) && $niv == $row->_niveau)) && !in_array($row->_niveau, $_SESSION["delete"]["Niveau"])) {
                $nivset = true;
            }
            if (((!isset($_GET["gdi"]) || $gdi == "") || (isset($_GET["gdi"]) && $gdi == $row->_domaine)) && !in_array($row->_domaine, $_SESSION["delete"]["Grande_Discipline"])) {

                $gdiset = true;
            }
            if (((!isset($_GET["dip"]) || $dip == "")  || (isset($_GET["dip"]) && $dip == $row->_diplome)) && !(in_array($row->_diplome, $_SESSION["delete"]["Diplome"]))) {

                $dipset = true;
            }
            if ($nivset && $dipset && $gdiset) {

                $formation = addSet($formation, ($row));

                $Discipline2 = addSet($Discipline2, $row->_discipline);
                $Niveau2 = addSet($Niveau2, $row->_niveau);
                $Grande_Disciple2 = addSet($Grande_Disciple2, $row->_domaine);
                $Diplome2 = addSet($Diplome2, $row->_diplome);

                $etab = addSet($etab, $row->_uai);
                $nbRow++;
            }
        } else if (!(in_array($row->_domaine, $_SESSION["delete"]["Grande_Discipline"]))) {

            $nivset = false;
            $disset = false;
            $dipset = false;
            if (((!isset($_GET["niv"]) || $niv == "")  || (isset($_GET["niv"]) && $niv == $row->_niveau)) && !in_array($row->_niveau, $_SESSION["delete"]["Niveau"])) {
                $nivset = true;
            }
            if (((!isset($_GET["dis"]) || $dis == "") || (isset($_GET["dis"]) && $dis == $row->_discipline)) && !(in_array($row->_discipline, $_SESSION["delete"]["Discipline"]))) {
                $disset = true;
            }
            if (((!isset($_GET["dip"]) || $dip == "")  || (isset($$_GET["dip"]) && $dip == $row->_diplome)) && !(in_array($row->_diplome, $_SESSION["delete"]["Diplome"]))) {
                $dipset = true;
            }
            if ($nivset && $dipset && $disset) {


                $formation = addSet($formation,  $row);
                $Discipline2 = addSet($Discipline2, $row->_discipline);
                $Niveau2 = addSet($Niveau2, $row->_niveau);
                $Grande_Disciple2 = addSet($Grande_Disciple2, $row->_domaine);
                $Diplome2 = addSet($Diplome2, $row->_diplome);

                $etab = addSet($etab, $row->_uai);
                $nbRow++;
            }
        } else if (!(in_array($row->_niveau, $_SESSION["delete"]["Niveau"]))) {
            $gdiset = false;
            $disset = false;
            $dipset = false;

            if (((!isset($_GET["gdi"]) || $gdi == "") || (isset($_GET["gdi"]) && $gdi == $row->_domaine)) && !in_array($row->_domaine, $_SESSION["delete"]["Grande_Discipline"])) {


                $gdiset = true;
            }
            if (((!isset($_GET["dis"]) || $dis == "") || (isset($_GET["dis"]) && $dis == $row->_discipline)) && !(in_array($row->_discipline, $_SESSION["delete"]["Discipline"]))) {


                $disset = true;
            }
            if (((!isset($_GET["dip"]) || $dip == "")  || (isset($_GET["dip"]) && $dip == $row->_diplome)) && !(in_array($row->_diplome, $_SESSION["delete"]["Diplome"]))) {


                $dipset = true;
            }
            if ($gdiset && $dipset && $disset) {


                $formation = addSet($formation,  $row);
                $Discipline2 = addSet($Discipline2, $row->_discipline);
                $Niveau2 = addSet($Niveau2, $row->_niveau);
                $Grande_Disciple2 = addSet($Grande_Disciple2, $row->_domaine);
                $Diplome2 = addSet($Diplome2, $row->_diplome);

                $etab = addSet($etab,  $row->_uai);
                $nbRow++;
            }
        } else if (!(in_array($row->_diplome, $_SESSION["delete"]["Diplome"]))) {
            $gdiset = false;
            $disset = false;
            $nivset = false;
            if (((!isset($_GET["niv"]) || $niv == "")  || (isset($_GET["niv"]) && $niv == $row->_niveau)) && !in_array($row->_niveau, $_SESSION["delete"]["Niveau"])) {
                $nivset = true;
            }
            if (((!isset($_GET["dis"]) || $dis == "")  || (isset($_GET["dis"]) && $dis == $row->_discipline)) && !in_array($row->_discipline, $_SESSION["delete"]["Discipline"])) {
                $disset = true;
            }
            if (((!isset($_GET["gdi"]) || $gdi == "") || (isset($_GET["gdi"]) && $gdi == $row->_domaine)) && !in_array($row->_domaine, $_SESSION["delete"]["Grande_Discipline"])) {
                $gdiset = true;
            }
            if ($gdiset && $nivset && $disset) {

                $formation = addSet($formation,  $row);
                $Discipline2 = addSet($Discipline2,  $row->_discipline);
                $Niveau2 = addSet($Niveau2, $row->_niveau);
                $Grande_Disciple2 = addSet($Grande_Disciple2, $row->_domaine);
                $Diplome2 = addSet($Diplome2, $row->_diplome);

                $etab = addSet($etab,  $row->_uai);
                $nbRow++;
            }
        }
    }
    if(count($formation)==0){
        header("Location: ./index.php");
    }
    $_SESSION["formation"] = $formation;
    $All2 = array(
        "Discipline" => $Discipline2,  "Grande_Discipline" => $Grande_Disciple2, "Niveau" => $Niveau2, "Diplome" => $Diplome2
    );
    $_SESSION["All2"] = $All2;
    $All = $_SESSION["All"];
} else if (isset($_GET["Valider"])) {

    //Si le premier filtrage à était fais
    $niv = $_GET["niv"];
    $dip = $_GET["dip"];
    $gdi = $_GET["gdi"];
    $dis = $_GET["dis"];
    $reg = $_GET["reg"];
    $dep = $_GET["dep"];
    $All = $_SESSION["All"];

    $link = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=-1&sort=-rentree_lib&facet=etablissement&facet=etablissement_lib&facet=diplome_lib&facet=niveau_lib&facet=gd_disciscipline_lib&facet=discipline_lib&facet=sect_disciplinaire_lib&facet=com_etab_lib&facet=dep_etab_lib&facet=reg_etab_lib&refine.rentree_lib=2017-18&apikey=cbd0f3f96f5d3140ff563d0b18d2b04c9d873c8e5d0b1a949b11eb90";
    if ($reg != null && $reg != "") {
        $link .= "&refine.reg_etab_lib=$reg";
    }
    if ($dep != null && $dep != "") {
        $link .= "&refine.dep_etab_lib=$dep";
    }
    if ($niv != null && $niv != "") {
        $link .= "&refine.niveau_lib=$niv";
    }
    if ($gdi != null && $gdi != "") {
        $link .= "&refine.gd_disciscipline_lib=$gdi";
    }
    if ($dis != null && $dis != "") {
        $link .= "&refine.sect_disciplinaire_lib=$dis";
    }
    if ($dip != null && $dip != "") {
        $link .= "&refine.diplome_lib=$dip";
    }
    $contents = lauchApi($link);

    $_SESSION["delete"] = null;

    $Discipline2 = array();
    $Etablissement2 = array();
    $Grande_Disciple2 = array();
    $Niveau2 = array();
    $Diplome2 = array();
    $formation = array();
    $etab = array();
    $nbRow = 0;


    foreach ($contents["records"] as $key2 => $row) {

        $formation = addSet($formation,  createFormation($row["fields"]));
        $Discipline2 = addSet($Discipline2, $row["fields"]["sect_disciplinaire_lib"]);
        $Niveau2 = addSet($Niveau2, $row["fields"]["niveau_lib"]);
        $Grande_Disciple2 = addSet($Grande_Disciple2, $row["fields"]["gd_disciscipline_lib"]);
        $Diplome2 = addSet($Diplome2, $row["fields"]["diplome_lib"]);
        $etab = addSet($etab, $row["fields"]["etablissement"]);
        $nbRow++;
    }

    $_SESSION["formation"] = $formation;
    $All2 = array(
        "Discipline" => $Discipline2,  "Grande_Discipline" => $Grande_Disciple2, "Niveau" => $Niveau2, "Diplome" => $Diplome2
    );
    $_SESSION["All2"] = $All2;

    $All = $_SESSION["All"];
} else {
    marker(true, true);
    //Partie première initialisation
    $link = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=-1&sort=-rentree_lib&facet=etablissement&facet=etablissement_lib&facet=diplome_lib&facet=niveau_lib&facet=gd_disciscipline_lib&facet=discipline_lib&facet=sect_disciplinaire_lib&facet=com_etab_lib&facet=dep_etab_lib&facet=reg_etab_lib&refine.rentree_lib=2017-18&apikey=cbd0f3f96f5d3140ff563d0b18d2b04c9d873c8e5d0b1a949b11eb90";

    $contents = lauchApi($link);

    $All = array(
        "Discipline" => array(), "Secteur" => array(),
        "Etablissement" => array(), "Grande_Disciple" => array(),
        "Commune" => array(), "Departement" => array(),
        "Region" => array(), "Niveau" => array(), "Diplome" => array(), "ComNUm" => array()
    );
    $etab = array();

    $_SESSION["All2"] = null; //pour que la liste de réponse ne sois pas visible si on rafrachit le tout

    foreach ($contents["records"] as $key2 => $row) {

        foreach ($row["fields"] as $key => $value) {

            if ($key == ("sect_disciplinaire_lib")) {

                $All["Discipline"] = addSet($All["Discipline"], $value);
                $etab = addSet($etab, $row["fields"]["etablissement"]);
            } else if ($key == ("sect_disciplinaire_lib")) {
                $All["Secteur"] = addSet($All["Secteur"], $value);
            } else if ($key == ("etablissement_lib")) {
                $All["Etablissement"] = addSet($All["Etablissement"], $value);

                $etab = addSet($etab, $row["fields"]["etablissement"]);
            } else if ($key == ("gd_disciscipline_lib")) {
                $All["Grande_Disciple"] = addSet($All["Grande_Disciple"], $value);
                $etab = addSet($etab, $row["fields"]["etablissement"]);
            } else if ($key == ("com_etab_lib")) {
                $All["Commune"] = addSet($All["Commune"], $value);
                $etab = addSet($etab, $row["fields"]["etablissement"]);
            } else if ($key == ("dep_etab_lib")) {
                $All["Departement"] = addSet($All["Departement"], $value);
                $etab = addSet($etab, $row["fields"]["etablissement"]);
            } else if ($key == ("reg_etab_lib")) {
                $All["Region"] = addSet($All["Region"], $value);
                $etab = addSet($etab, $row["fields"]["etablissement"]);
            } else if ($key == ("niveau_lib")) {
                $All["Niveau"] = addSet($All["Niveau"], $value);
                $etab = addSet($etab, $row["fields"]["etablissement"]);
            } else if ($key == ("diplome_lib")) {
                $All["Diplome"] = addSet($All["Diplome"], $value);
                $etab = addSet($etab, $row["fields"]["etablissement"]);
            } else if ($key == "com_ins") {
                $All["ComNUm"] = addSet($All["ComNUm"], $value);
                $etab = addSet($etab, $row["fields"]["etablissement"]);
            }
        }
    }
    sort($All["Discipline"]);
    sort($All["Secteur"]);
    sort($All["Etablissement"]);
    sort($All["Grande_Disciple"]);
    sort($All["Commune"]);
    sort($All["Departement"]);
    sort($All["Region"]);
    sort($All["Niveau"]);
    sort($All["Diplome"]);
    sort($All["ComNUm"]);
    //On initialise une liste de avec toutes les possibilités
    $_SESSION["etab"] = $etab;
    $_SESSION["All"] = $All;
}


?>


<body>
    <div class="main">
        <div class="Recherche">
            <div class="title1">
                Trouve ta formation
            </div>
            <div class="search">
                <form action="#" method="GET">
                    <div class="wrapColumn">
                        <div>
                            <input list="niv" class="inputData" name="niv" placeholder="Niveau" />
                            <datalist id="niv">
                                <?php
                                foreach ($All["Niveau"] as $key2) {
                                    echo "   <option value=\"$key2\" ></option>";
                                }
                                ?>
                            </datalist>
                        </div>
                        <div>

                            <input list="dip" class="inputData" name="dip" placeholder="Formation" />
                            <datalist id="dip">
                                <?php
                                foreach ($All["Diplome"] as $key2) {
                                    echo "   <option value=\"$key2\" ></option>";
                                }
                                ?>
                            </datalist>
                        </div>
                    </div>

                    <div class="wrapColumn">
                        <div>
                            <input list="reg" class="inputData" name="reg" placeholder="Région" />
                            <datalist id="reg">
                                <?php
                                foreach ($All["Region"] as $key2) {
                                    echo "   <option value=\"$key2\" ></option>";
                                }
                                ?>
                            </datalist></div>
                        <div> <input list="dep" class="inputData" name="dep" placeholder="Département" />
                            <datalist id="dep">
                                <?php
                                foreach ($All["Departement"] as $key2) {
                                    echo "   <option value=\"$key2\" ></option>";
                                }
                                ?>
                            </datalist></div>
                    </div>
                    <div class="wrapColumn">


                        <div>
                            <input list="gdi" class="inputData" name="gdi" placeholder="Domaine" />
                            <datalist id="gdi">
                                <?php
                                foreach ($All["Grande_Disciple"] as $key2) {
                                    echo "   <option value=\"$key2\" ></option>";
                                }
                                ?>
                            </datalist>
                        </div>
                        <div><input list="dis" class="inputData" name="dis" placeholder="Spécialité" />
                            <datalist id="dis">
                                <?php
                                foreach ($All["Discipline"] as $key2) {
                                    echo "   <option value=\"$key2\" ></option>";
                                }
                                ?>
                            </datalist></div>

                    </div>
                    <input type="submit" value="Valider" name="Valider" class="inputFinal">
                </form>

                <?php
                if (isset($nbRow)) {
                    $size = count($_SESSION["formation"]);
                    echo " <p>Résultat $size</p>";
                }
                ?>
                <table class="GroupSearch">
                    <tr>
                        <td>
                            <h2>Niveau</h2>
                            <div class="group">
                                <?php
                                if (isset($All2["Niveau"])) {
                                    if ($All2["Niveau"]) {

                                        foreach ($All2["Niveau"] as $key2) {
                                            echo "  <div class=\"wrap\">
                                        <div><a href=\"#\">$key2</a></div>
                                        <div>
                                        <form action=\"#\" method=\"GET\">       
                                        <input type=\"submit\" value=\"X\" name=\"Delete\" class=\"inputDel\">
                                        <input type=\"hidden\" value=\"Niveau\" name=\"deleteType\" >
                                        <input type=\"hidden\" value=\"$key2\" name=\"delete\" ></form></div>
                                    </div>
                             
                                <hr>";
                                        }
                                    }
                                }
                                ?>
                            </div>

                        <td>
                            <h2>Formation</h2>
                            <div class="group">

                                <?php
                                if (isset($All2["Diplome"])) {

                                    foreach ($All2["Diplome"] as $key2) {
                                        echo "  <div class=\"wrap\">
                                        <div><a href=\"#\">$key2</a></div>
                                        <div>
                                        <form action=\"#\" method=\"GET\">       
                                        <input type=\"submit\" value=\"X\" name=\"Delete\" class=\"inputDel\">
                                        <input type=\"hidden\" value=\"Diplome\" name=\"deleteType\" >
                                        <input type=\"hidden\" value=\"$key2\" name=\"delete\" ></form></div>
                                    </div>
                             
                                <hr>";
                                    }
                                }
                                ?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h2>Domaine</h2>
                            <div class="group">

                                <?php
                                if (isset($All2["Grande_Discipline"])) {

                                    foreach ($All2["Grande_Discipline"] as $key2) {
                                        echo "
                                        <div class=\"wrap\">
                                            <div><a href=\"#\">$key2</a></div>
                                            <div>
                                            <form action=\"#\" method=\"GET\">       
                                            <input type=\"submit\" value=\"X\" name=\"Delete\" class=\"inputDel\">
                                            <input type=\"hidden\" value=\"Grande_Discipline\" name=\"deleteType\" >
                                            <input type=\"hidden\" value=\"$key2\" name=\"delete\" ></form></div>
                                        </div>
                                 
                                    <hr>";
                                    }
                                }
                                ?>
                            </div>
                        </td>
                        <td>
                            <h2>Spécialité</h2>
                            <div class="group">
                                <?php
                                if (isset($All2["Discipline"])) {

                                    foreach ($All2["Discipline"] as $key2) {
                                        echo "   <div class=\"wrap\">
                                        <div><a href=\"#\">$key2</a></div>
                                        <div>
                                        <form action=\"#\" method=\"GET\">       
                                        <input type=\"submit\" value=\"X\" name=\"Delete\" class=\"inputDel\">
                                        <input type=\"hidden\" value=\"Discipline\" name=\"deleteType\" >
                                        <input type=\"hidden\" value=\"$key2\" name=\"delete\" ></form></div>
                                    </div>
                             
                                <hr>";
                                    }
                                }
                                ?>

                            </div>
                        </td>
                    </tr>


                </table>

            </div>



        </div>
        <div class="Map">
            <div id="mapid">

                <script>
                    var mymap = L.map('mapid').setView([46.22, 2.21], 5);

                    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
                        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                        maxZoom: 18,
                        id: 'mapbox.streets',
                        accessToken: 'pk.eyJ1IjoiYXp1a2lybyIsImEiOiJjazM1dzJkeXExZnByM21vMmZmcGl2cjk2In0.veVispK9Q1Yo8HJc39iZlw'
                    }).addTo(mymap);
                </script>
                <?php
                $link = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-etablissements-enseignement-superieur&facet=uai&facet=type_d_etablissement&facet=com_nom&facet=dep_nom&facet=aca_nom&facet=reg_nom&facet=pays_etranger_acheminement&rows=-1&q=";
                $q = "";

                foreach ($etab as $uai) {
                    $q .= "uai%3D$uai+OR+";
                }


                $q .= "uai%3D1";
                $link .= $q;
                $contents = lauchApi($link);
                foreach ($contents["records"] as $key => $value) {

                    marker($value["fields"]);
                }



                ?>

            </div>

            <button id="geol">Géolocalisation</button>


        </div>
        <div class="Formation">
            <?php
            if (!isset($_GET["paginationNumber"])) {
                $_GET["paginationNumber"] = 1;
            }
            if (isset($_GET["pred"]) &&  $_GET["paginationNumber"] > 1) {
                $_GET["paginationNumber"] -= 1;
            } else if (isset($_GET["Suiv"])) {
                $_GET["paginationNumber"] += 1;
            }
            if (isset($_SESSION["All2"])) {
                if (lastFormation($_SESSION["All2"])) {
                    $i = 0;
                    foreach ($_SESSION["formation"] as $key => $value) {

                        $i++;
                        if ($i < $_GET["paginationNumber"] * 2) {
                            continue;
                        }
                        if ($i >= ($_GET["paginationNumber"] + 1) * 2) {

                            break;
                        }
                        echo "<form action='.' method='GET'>";
                        foreach ($_GET as $name => $value2) {
                            $name = htmlspecialchars($name);

                            $value2 = htmlspecialchars($value2);

                            if ($name == "Suiv" || $name == "pred"  || $name == "paginationNumber" || $name == "uaiInc") {

                                continue;
                            }
                            echo '<input type="hidden" name="' . $name . '" value="' . $value2 . '">';
                        }
                        echo "<input type='hidden' name='uaiIncWebSite' value='$value->_url' style='color:$value->_color'>";
                        echo "<input type='hidden' name='uaiInc' value='$value->_uai' style='color:$value->_color'>";
                        echo "<input type='submit' name='u' value=\"".$value->_etablissement."\" style='background:$value->_color'></form>";
                        
                        
                        echo $value->toString();
                    }
                }
            }
            ?>
            <div>
                <?php
                if (isset($_SESSION["All2"])) {
                    if (lastFormation($_SESSION["All2"])) {
                        $pag = $_GET["paginationNumber"];
                        $maxPag = round(count($_SESSION["formation"]) / 2);
                        echo "
                <center>
            <form method='GET' action='.' style='width:100%'> 
            <input type='hidden' name='paginationNumber'value='$pag'>
            <input type='submit' name='pred' value='Précédent'>
            <div>$pag / $maxPag </div>
            <input type='submit' name='Suiv' value='Suivant' style='   text-align: center; '>";

                        foreach ($_GET as $name => $value) {
                            $name = htmlspecialchars($name);

                            $value = htmlspecialchars($value);

                            if ($name == "Suiv" || $name == "pred"  || $name == "paginationNumber") {

                                continue;
                            }
                            echo '<input type="hidden" name="' . $name . '" value="' . $value . '">';
                        }

                        echo "</form>
            </center>";
                    }
                }

                ?>
            </div>

        </div>
    </div>
    <div class="footer">
        <a href="https://bitbucket.org/azukiro/opendata/src/master/" target="_blank">Lien Bitbucket du projet</a>
    </div>
</body>

<script>
    document.getElementById("geol").addEventListener("click", function() {
        mymap.locate({
            setView: true,
            maxZoom: 13
        });
    });
</script>

</html>
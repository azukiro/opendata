<?php




class Formation{
    public $_discipline;
    public $_domaine;
    public $_diplome;
    public $_niveau;
    public $_commune;
    public $_departement;
    public $_region;
    public $_effectif;
    public $_url;
    public $_etablissement;
    public $_uai;
    public $_color;

    function __construct($_discipline,$_domaine,$_diplome,$_niveau,$_commune,$_departement,$_region,$_effectif,$_url,$_etablissement,$_uai)
    {
    
        $this->_discipline=$_discipline;
        $this->_domaine=$_domaine;
        $this->_diplome=$_diplome;
        $this->_niveau=$_niveau;
        $this->_commune=$_commune;
        $this->_departement=$_departement;
        $this->_region=$_region;
        $this->_effectif=$_effectif;
        $this->_url=$_url;
        $this->_etablissement=$_etablissement;
        $this->_uai=$_uai;
              //Create a color associate to the university with the uai
        $r=substr($this->_uai,0,2);
        $g=substr($this->_uai,2,2);
        $b=substr($this->_uai,4,2);
        $this->_color="#$g$b$r";
    }

    public function toString(){
        //Fonction for print formation 
        include("parametre.inc.php");
        try{
            $connexion="mysql:host=$host;dbname=$db;charset=utf8;";
            $bd = new PDO($connexion, $user, $pwd,array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION)); 
        }catch(Exception $e){
            die('Connexion impossible à la base de données !'.$e->getMessage());
        }
        
  
      
        $select="SELECT nbClick FROM universite WHERE uai='$this->_uai';";
        $result=$bd->query($select);
  
        $nb=0;
        while($ligne=$result->fetch(PDO::FETCH_NUM)){
            $nb=$ligne[0];
        }

        return "<li> b vue :" . $nb . "</li>
        <li> Discipline :" . $this->_discipline . "</li>
        <li> Domaine :" . $this->_domaine. "</li>
        <li> Niveau :" . $this->_niveau . "</li>
        <li> Diplome :" . $this->_diplome . "</li>
        <li> Commune :" . $this->_commune . "</li>
        <li> Departement :" . $this->_departement . "</li>
        <li> Region :" . $this->_region . "</li>
        <li> Effectif :" . $this->_effectif . "</li>
        </ul><hr>";
    }

    
    
}
